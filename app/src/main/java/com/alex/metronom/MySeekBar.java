package com.alex.metronom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class MySeekBar extends SeekBar {
    public MySeekBar(Context context) {
        super(context);
    }

    public MySeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MySeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStrokeWidth(2);
        paint.setColor(MainActivity.getColor(getContext(), R.color.colorSeekBarBack));
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(0, this.getHeight() / 2);
        path.lineTo(this.getWidth(), this.getHeight() / 4);
        path.lineTo(this.getWidth(), this.getHeight() / 4 + this.getHeight() / 2);
        path.lineTo(0, this.getHeight() / 2);
        path.close();

        canvas.drawPath(path, paint);

        super.onDraw(canvas);
    }
}
