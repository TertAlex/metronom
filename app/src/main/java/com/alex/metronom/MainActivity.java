package com.alex.metronom;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    public static final int METRONOM_ON = 1001;
    public static final int METRONOM_OFF = 1002;
    public static final String PARAM_PINTENT = "pendingIntent";
    public static final int DEFAULT_INTERVAL = 1000;

    public static String TAG = "Mentonom_Logs";
    Intent intentServise;
    MetronomServise metronomServise;
    boolean bound = false;
    ServiceConnection sConn;
    SeekBar sbInterval;
    TextView etInterval;
    ImageView ivIndicator;
    ToggleButton btnVibr, btnFlash, btnSound, btnStart;
    int currentInterval = DEFAULT_INTERVAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PendingIntent pi = createPendingResult(0, new Intent(), 0);

        this.intentServise = new Intent(this, MetronomServise.class).putExtra(PARAM_PINTENT, pi);
        this.sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(TAG, "MainActivity onServiceConnected");
                metronomServise = ((MetronomServise.MetronomBinder) binder).getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "MainActivity onServiceDisconnected");
                bound = false;
            }
        };

        startService(this.intentServise);
        bindService(this.intentServise, this.sConn, BIND_AUTO_CREATE);

        this.btnStart = (ToggleButton) findViewById(R.id.btnStart);
        this.btnVibr = (ToggleButton) findViewById(R.id.btnVibr);
        this.btnFlash = (ToggleButton) findViewById(R.id.btnFlash);
        this.btnSound = (ToggleButton) findViewById(R.id.btnSound);
        this.btnStart.setOnClickListener(this);
        this.btnVibr.setOnClickListener(this);
        this.btnFlash.setOnClickListener(this);
        this.btnSound.setOnClickListener(this);

        this.sbInterval = (SeekBar) findViewById(R.id.sbInterval);
        if (this.sbInterval != null) {
            this.sbInterval.setOnSeekBarChangeListener(this);
        }

        this.etInterval = (TextView) findViewById(R.id.etInterval);
        this.etInterval.setText(String.valueOf(DEFAULT_INTERVAL));
        this.etInterval.setOnClickListener(this);

        this.ivIndicator = (ImageView) findViewById(R.id.ivIndicator);
        this.ivIndicator.setBackgroundResource(R.drawable.indicator_off);

        //  findViewById(R.id.btnVibr).getBackground().setAlpha(153);
        //  findViewById(R.id.btnFlash).getBackground().setAlpha(153);
    }

    @Override
    protected void onDestroy() {
        if (!this.bound) return;
        unbindService(this.sConn);
        this.bound = false;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == METRONOM_ON) {
            this.ivIndicator.setBackgroundResource(R.drawable.indicator_on);
        } else if (resultCode == METRONOM_OFF) {
            this.ivIndicator.setBackgroundResource(R.drawable.indicator_off);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                this.metronomServise.manageMetronom(this.btnStart.isChecked());
                break;
            case R.id.btnVibr:
                this.metronomServise.setIsVibrOn(this.btnVibr.isChecked());
                break;
            case R.id.btnFlash:
                this.metronomServise.setIsFlashOn(this.btnFlash.isChecked());
                break;
            case R.id.btnSound:
                this.metronomServise.setIsSoundOn(this.btnSound.isChecked());
                break;
            case R.id.etInterval:
                ShowDialogEditInterval();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        this.etInterval.setText(String.valueOf(progress * 100));
        this.metronomServise.setInterval(progress * 100);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            //noinspection deprecation
            return context.getResources().getColor(id);
        }
    }

    private void ShowDialogEditInterval() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.llMain);
        View promptsView = li.inflate(R.layout.dialog_enter_bpm, viewGroup, false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        userInput.setSingleLine(true);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getString(R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                saveInterval(Integer.parseInt(userInput.getText().toString()));
                            }
                        })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        userInput.setText(String.valueOf(this.currentInterval));
        userInput.setSelection(userInput.getText().length());

        // show it
        alertDialog.show();
    }

    private void saveInterval(int interval) {
        if (interval > 5000) interval = 5000;
        if (interval < 0) interval = 0;
        sbInterval.setProgress(interval / 100);
        this.currentInterval = interval;
    }

}
