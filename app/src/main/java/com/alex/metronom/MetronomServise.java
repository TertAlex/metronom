package com.alex.metronom;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Binder;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MetronomServise extends Service {
    MetronomBinder binder;
    Timer timer;
    TimerTask tTask;
    long interval = MainActivity.DEFAULT_INTERVAL;
    PendingIntent pi;
    Camera cam = null;
    Camera.Parameters p;
    Vibrator vibrator;
    ToneGenerator toneG;
    boolean isVibrOn = true, isFlashOn = true, isSoundOn = true;
    boolean isRun = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.binder = new MetronomBinder();
        this.timer = new Timer();
        this.vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        this.toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        try {
            if (getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {
                cam = Camera.open();
                p = cam.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Exception flashLightOn()",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)) {
                cam.release();
                cam = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Exception flashLightOff",
                    Toast.LENGTH_SHORT).show();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pi = intent.getParcelableExtra(MainActivity.PARAM_PINTENT);
        return super.onStartCommand(intent, flags, startId);
    }

    void schedule() {
        if (this.tTask != null) this.tTask.cancel();
        if (this.interval > 0) {
            this.tTask = new TimerTask() {
                public void run() {
                    try {
                        pi.send(MainActivity.METRONOM_ON);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                    flashLightOn();
                    soundOn();
                    vibrationOn();

                    Log.e(MainActivity.TAG, "run " + interval);
                    try {
                        TimeUnit.MILLISECONDS.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    try {
                        pi.send(MainActivity.METRONOM_OFF);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                }
            };
            if (this.isRun) {
                this.timer.schedule(this.tTask, 500, this.interval);

            }
        }
    }

    private void vibrationOn() {
        if (isVibrOn) {
            new Thread(new Runnable() {
                public void run() {
                    vibrator.vibrate(200);
                }
            }).start();
        }
    }

    private void flashLightOn() {
        if (isFlashOn) {
            new Thread(new Runnable() {
                public void run() {
                    cam.setParameters(p);
                    cam.startPreview();
            /*        try {
                        TimeUnit.MILLISECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    cam.stopPreview();
                }
            }).start();
        }
    }

    private void soundOn() {
        if (isSoundOn) {
            new Thread(new Runnable() {
                public void run() {
                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                }
            }).start();
        }
    }

    void setInterval(long interval) {
        this.interval = interval;
        this.schedule();
    }

    void manageMetronom(boolean isRun) {
        this.isRun = isRun;
        if (isRun) {
            this.schedule();
        } else {
            if (this.tTask != null) this.tTask.cancel();
        }
    }

    public void setIsVibrOn(boolean isVibrOn) {
        this.isVibrOn = isVibrOn;
    }

    public void setIsFlashOn(boolean isFlashOn) {
        this.isFlashOn = isFlashOn;
    }

    public void setIsSoundOn(boolean isSoundOn) {
        this.isSoundOn = isSoundOn;
    }

    class MetronomBinder extends Binder {
        MetronomServise getService() {
            return MetronomServise.this;
        }
    }
}
