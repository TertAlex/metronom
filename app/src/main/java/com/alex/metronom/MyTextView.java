package com.alex.metronom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

public class MyTextView extends TextView {
    public MyTextView(Context context) {
        super(context);
        this.setCustomFont();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setCustomFont();
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setCustomFont();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        String temp = text + " " + getResources().getText(R.string.bpm);
        super.setText(temp, type);
    }

    public void setCustomFont() {
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 36);

    }
}
